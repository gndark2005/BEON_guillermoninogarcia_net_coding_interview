﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;
using SecureFlight.Core.Interfaces;

namespace SecureFlight.Core.Services;

public class BaseService<TEntity> : IService<TEntity>
    where TEntity : class
{
    private readonly IRepository<TEntity> _repository;

    public BaseService(IRepository<TEntity> repository)
    {
        _repository = repository;
    }
    public async Task<OperationResult<IReadOnlyList<TEntity>>> GetAllAsync()
    {
        return new OperationResult<IReadOnlyList<TEntity>>(await _repository.GetAllAsync());
    }

    public async Task<OperationResult<IReadOnlyList<TEntity>>> FilterAsync(Expression<Func<TEntity, bool>> predicate)
    {
        return new OperationResult<IReadOnlyList<TEntity>>(await _repository.FilterAsync(predicate));
    }

    public void AddNew(TEntity entidad)
    {
        throw new NotImplementedException();
    }

    public TEntity update(TEntity entidad)
    {
        throw new NotImplementedException();
    }
}